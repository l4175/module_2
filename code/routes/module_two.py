from fastapi import APIRouter, Response, status
from config.db import conn
from models.models import module_two
from schemas.module_schemas import Module_Two, Transcription_Count
from cryptography.fernet import Fernet
from starlette.status import HTTP_204_NO_CONTENT, HTTP_100_CONTINUE, HTTP_505_HTTP_VERSION_NOT_SUPPORTED, HTTP_400_BAD_REQUEST
from typing import List
from sqlalchemy import func, select

key = Fernet.generate_key()
f = Fernet(key)
ModuleTwo = APIRouter()


@ModuleTwo.get("/Transcriptlist/{id}", tags=["Transcription list in the module two"],
    response_model=List[Module_Two],
    description="Get a list of all transcription of specific audio",)
def get_transcriptions(id:str):
    return conn.execute(module_two.select().where(module_two.c.id==id)).fetchall()

@ModuleTwo.get("/Transcript/count", tags=["Count transcription"], response_model=Transcription_Count)
def get_transcriptions_count():
    result = conn.execute(select([func.count()]).select_from(Module_Two))
    return {"total": tuple(result)[0][0]}


@ModuleTwo.put("/Transcript/{id}", 
          tags=["Update of correct or incorrect transcription"], 
          response_model=Module_Two, 
          description="Update if a transcribed sentence is incorrect"
         )
def update_user(id:str):
    conn.execute(module_two.update().values(to_correct=True).where(module_two.c.id ==id))
    return conn.execute(module_two.select().where(module_two.c.id==id)).first()

@ModuleTwo.get("/")
def helloworld():
    return "Hello! This is a Transcription Module API rest of 'Licenciamiento de Cámara Project."

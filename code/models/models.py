from tokenize import String
from sqlalchemy import Table,Column
from sqlalchemy.sql.sqltypes import Integer,  String, Boolean
from config.db import meta, engine
from sqlalchemy import (
    Enum, ForeignKeyConstraint, PrimaryKeyConstraint, CheckConstraint, UniqueConstraint, Table,
    Column, Float)
from sqlalchemy.schema import ForeignKey



module_two = Table("module_two", meta, 
               Column("id", Integer, primary_key = True),
               Column("minute_initial", String(255)), 
               Column("minute_final", String(255)),
               Column("id_audio", Integer,ForeignKey("audio.id")),
               Column("text_intial", String, index=True),
               Column("corrected_intial", String, index=True),
               Column("text_final", String, index=True),
               Column("id_session", Integer,ForeignKey("session.id")),
               Column("FK_u_asignado", Integer,ForeignKey("users.id")),
               Column("to_correct", Boolean, default=True)
               )

meta.create_all(engine)
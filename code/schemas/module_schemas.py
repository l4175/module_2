from pydantic import BaseModel
from typing import Optional



class Module_Two(BaseModel):
    id: Optional[int]
    minute_initial:str
    minute_final:str
    id_audio:int
    text_intial: str
    corrected_intial: str
    text_final:str
    id_session:int
    FK_u_asignado:int
    to_correct:bool




class Transcription_Count(BaseModel):
    total: int
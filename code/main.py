
from fastapi import FastAPI
from routes.module_two import ModuleTwo
from config.openapi import tags_metadata
from starlette.middleware.cors import CORSMiddleware

app = FastAPI(
    title="Module Two API",
    description="This is a quality module REST API using python and mysql",
    version="0.0.1",
    openapi_tags=tags_metadata,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


app.include_router(ModuleTwo)